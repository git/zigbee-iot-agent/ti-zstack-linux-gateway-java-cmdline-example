/**************************************************************************************************
  Filename:       CommandLine.java
  Revised:        $$
  Revision:       $$

  Description:    CommandLine class

  Copyright (C) {2014} Texas Instruments Incorporated - http://www.ti.com/


   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

     Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the
     distribution.

     Neither the name of Texas Instruments Incorporated nor the names of
     its contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
**************************************************************************************************/

package com.example;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.example.CommandLineCmd;
import com.google.protobuf.ByteString;

public class CommandLine {	
		
	private Scanner inputReader;	
	List<CommandLineCmd> cmdList;
	
	CommandLine() {
		inputReader = new Scanner(System.in);
		inputReader.useDelimiter("\n|\r");

		cmdList = new ArrayList<CommandLineCmd>();				
	}
	
	void addCmd(CommandLineCmd cmd)
	{
		cmdList.add(cmd);
	}
	
	public boolean waitPress(int duration) {
		// TODO Auto-generated method stub
		return false;
	}

	public CommandLineCmd getCmd() {
		
		String cmdString = new String();
		
		while(true)
		{
			print(">" + cmdString);
			cmdString = cmdString + inputReader.nextLine();
			
			List<CommandLineCmd> possibleCmds = new ArrayList<CommandLineCmd>();
			
			//if user types help here list all commands
			if( cmdString.equalsIgnoreCase("help") ) 
			{
				for(int cmdIdx = 0; cmdIdx < cmdList.size(); cmdIdx++)
				{
					printLn(cmdList.get(cmdIdx).cmdName + ":");
					printLn(cmdList.get(cmdIdx).cmdDescription);
/*					
					print("Parameters:");
					for(int paramIdx = 0; paramIdx < cmdList.get(cmdIdx).params.size(); paramIdx++)
					{
						print(cmdList.get(cmdIdx).params.get(paramIdx).paramName + ":"); 
						print(cmdList.get(cmdIdx).params.get(paramIdx).paramDescription);
						print("\n");
					}
*/					
				}
				cmdString = "";
			}
			else if((cmdString.equalsIgnoreCase("")) ||
				(cmdString.equalsIgnoreCase("\r")) )
			{
				//do nothing
			}
			else
			{			
				//Search for command sub string or match
				for(int cmdIdx = 0; cmdIdx < cmdList.size(); cmdIdx++)
				{
					if(cmdList.get(cmdIdx).cmdName.contains(cmdString))
					{
						possibleCmds.add(cmdList.get(cmdIdx));
					}
				}
				
				//where there no substring match
				if(possibleCmds.size() == 0)
				{
					//set the command to null so invalid text 
					//is not printed as the start of the command
					cmdString = "";
				}
				//was there only 1 substring our match
				else if(possibleCmds.size() == 1)
				{
					//was it not an exact match
					if(!possibleCmds.get(0).cmdName.equals(cmdString))
					{
						//print partially matched command
						printLn(possibleCmds.get(0).cmdName);
					}
					
					//get the parameters
					getParams(possibleCmds.get(0));
					
					//return the command
					return possibleCmds.get(0);
				}
				else
				{
					//print possible
					for(int cmdIdx = 0; cmdIdx < possibleCmds.size(); cmdIdx++)
					{
						printLn(possibleCmds.get(cmdIdx).cmdName);
					}				
					
					//fill in the reset of the command for common chars
					boolean charMatch = true;
					int charIdx= cmdString.length();
					char matchChar;
					while(charMatch)
					{			
						//check charIdx is less then string length of first possible cmd
						if(charIdx < possibleCmds.get(0).cmdName.length())
						{
							//store next char of the first cmd
							matchChar = possibleCmds.get(0).cmdName.charAt(charIdx);
							//index through possible cmd's checking if the matchCar is common
							for(int cmdIdx = 1; cmdIdx < possibleCmds.size(); cmdIdx++)
							{
								//first check the possible command has another char						
								if(charIdx < possibleCmds.get(cmdIdx).cmdName.length())
								{
									//Check that this possible command has the char common with the first
									if(matchChar != possibleCmds.get(cmdIdx).cmdName.charAt(charIdx))
									{
										//if not st the match to false and stop checking
										charMatch = false;
									}									
								}
							}
							
							//if this char match copy it to the cmdString
							if(charMatch)
							{
								cmdString += matchChar;
							}							
							
							//index to next char
							charIdx++;							
						}
					}
				}
			}
		}
	}
	
	public CommandLineCmd getParams(CommandLineCmd cmd) {
		if(cmd.params != null)
		{
			//Index through Cmd Params
			int paramIdx = 0;		
			while(paramIdx < cmd.params.size())
			{
				printLn("Input " + cmd.params.get(paramIdx).paramName + ":");
				{
					cmd.params.get(paramIdx).paramValue = inputReader.nextLine();
					
					//did the user type help or nothing
					if( (cmd.params.get(paramIdx).paramValue.equalsIgnoreCase("help")) ||
						(cmd.params.get(paramIdx).paramValue.equalsIgnoreCase("")) ||
						(cmd.params.get(paramIdx).paramValue.equalsIgnoreCase("\r")) )
					{
						//print the param description
						printLn("Param Description - \n" + cmd.params.get(paramIdx).paramDescription);
					}
					else
					{
						//Got value go to the next param
						paramIdx++;
					}
				}
			}
		}		
		return cmd;
	}
	
	public void printLn(String string) {
		System.out.println(string);			
	}

	public void print(String string) {
		System.out.print(string);			
	}
	
	public String getString(String prompt) {
		printLn(prompt);
		return inputReader.nextLine();
	}

	public long getIeeeParam(String param) {
		ByteString bsIeee;			
		bsIeee = getByteStringParam(param, 8);														
		return bsIeee.asReadOnlyByteBuffer().asLongBuffer().get();
	}

	public byte getByteParam(String param) {
		ByteString bsByte;			
		bsByte = getByteStringParam(param, 1);														
		return bsByte.asReadOnlyByteBuffer().get();
	}
	
	public ByteString getByteStringParam(String param, int numBytes) {
		byte[] byteBuf = new byte[numBytes]; 		

		//remove "0x"
		param = param.replaceAll("0x", "");
		//remove all non hex digits		
		param = param.replaceAll("[^A-Fa-f0-9]", "");

		//Index through each char string converting every 2 chars to a byte
		for(int paramCharIndex = 0; paramCharIndex < (2*numBytes); paramCharIndex+=2)
		{		
			//are there 2 digits in string
			if((paramCharIndex+1) < param.length())
			{
				//copy upper and lower nibble
				byteBuf[paramCharIndex / 2] = 
					(byte) ((Character.digit(param.charAt(paramCharIndex), 16) << 4) +
						   (Character.digit(param.charAt(paramCharIndex+1), 16)));
			}
			//Only 1 digit
			else if(paramCharIndex < param.length())
			{
				//copy digit to lower nibble
				byteBuf[paramCharIndex / 2] += 
					(byte) (Character.digit(param.charAt(paramCharIndex), 16));
			}
		}		
		
		return ByteString.copyFrom(byteBuf, 0, numBytes);
	}

	public BigInteger getBigIntParam(String param) {
		int base;
		
		if( param.contains("0x") )
		{ 
			base = 16;				
		}
		else
		{
			base = 10;
		}
		
		//remove all non hex digits
		param = param.replaceAll("[^A-Fa-f0-9/-]", "");
		return new BigInteger(param, base);
	}
	
	public int getIntParam(String param) {
		return getBigIntParam(param).intValue();
	}
	
	public short getShortParam(String param) {				
		return getBigIntParam(param).shortValue();
	}	
}
